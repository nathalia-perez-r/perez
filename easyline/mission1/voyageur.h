#ifndef VOYAGEUR_H
#define VOYAGEUR_H
#include <string>
using namespace std; 

//Creation de class Voyageur
class Voyageur
{

    private:
    //Définition d'attributs
    string nom;
    int age;
    string categorie;

    public:
    //Constructeurs 
    Voyageur();
    Voyageur(string nom, int age, string categorie);
    //Declaration des accesseurs
    string getNom();
    int getAge();
    string getCategorie();
    //Declaration des modificateurs
    void setNom(string nom);
    void setAge(int age);
    //Methode d'affichage
    void afficherDonnees();
};

#endif//VOYAGEUR_H