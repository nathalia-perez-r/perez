#include <iostream>
#include <string>
#include <vector>
#include <string>
#include <array>
#include "Compagnie.hpp"
using namespace std;

string motDePasse = "compagnia";

Compagnie::Compagnie(string nom)
{
  this->nom = nom;
}
Compagnie::Compagnie(string nom, string couleur, string couleurSecondaire)
{
  this->nom = nom;
  this->couleur = couleur;
  this->couleurSecondaire = couleurSecondaire;
}
Compagnie::Compagnie(string nom, int code, string couleur, string couleurSecondaire)
{
  this->nom = nom;
  this->code = code;
  this->couleur = couleur;
  this->couleurSecondaire = couleurSecondaire;
}
Compagnie::Compagnie() {}
void Compagnie::afficherDonees()
{
  cout << "Compagnie: " << this->nom << "\n"
       << this->code << "\n"
       << this->couleur << "\n"
       << this->couleurSecondaire << endl;
};
void Compagnie::setNom(string nom)
{
  this->nom = nom;
}
void Compagnie::setCode(int code)
{
  string motDePasseUtilisateur;
  while (motDePasse != motDePasseUtilisateur)
  {
    cout << "Indiquez le mot de passe pour modifier le code la compagnie: ";
    cin >> motDePasseUtilisateur;
  }
  this->code = code;
}
void Compagnie::setCouleur(string couleur)
{
  this->couleur = couleur;
}
void Compagnie::setCouleurSecondaire(string couleur)
{
  this->couleurSecondaire = couleur;
}
void Compagnie::setChiffreDAffaires(int CA)
{
  string motDePasseUtilisateur;
  while (motDePasse != motDePasseUtilisateur)
  {
    cout << "Indiquez le mot de passe pour modifier le chiffre d'affaire de la compagnie: ";
    cin >> motDePasseUtilisateur;
  }
  this->chiffreDAffaires = CA;
}
//Acceseurs
int Compagnie::getCode()
{
  string motDePasseUtilisateur;
  while (motDePasse != motDePasseUtilisateur)
  {
    cout << "Indiquez le mot de passe pour obtenir le code la compagnie: ";
    cin >> motDePasseUtilisateur;
  }
  return this->code;
}
string Compagnie::getCouleur() { return this->couleur; }
string Compagnie::getCouleurSecondaire()
{
  return this->couleurSecondaire;
  cout << "second";
}

ostream &operator<<(ostream &srtm, const Compagnie &v)
{
  return srtm << v.nom << " " << v.code << " " << v.couleur << " " << v.couleurSecondaire << " " << v.chiffreDAffaires << "\n";
}

int main()
{
  Compagnie compagnie1;
  compagnie1.setNom("jo");
  compagnie1.setCode(234);
  compagnie1.setCouleur("orange");
  compagnie1.setCouleurSecondaire("vert");
  compagnie1.afficherDonees();
  //Compagnie3------------------------------------------
  Compagnie compagnie3;
  compagnie3.setNom("easyJune");
  compagnie3.setCode(154);
  compagnie3.setCouleur("rouge");
  compagnie3.afficherDonees();
  //Compagnie4------------------------------------------
  Compagnie compagnie4("rex");
  compagnie4.setCode(123);
  compagnie4.setCouleur("violet");
  compagnie4.afficherDonees();
  //Compagnie5-----------------------------------------
  Compagnie compagnie5;
  compagnie5.setNom("xx");
  compagnie5.setCode(567);
  compagnie5.setCouleur("noir");
  compagnie5.afficherDonees();
  //Compagnie6------------------------------------------
  Compagnie compagnie6("lan", "rouge", "vert");
  compagnie6.afficherDonees();
  cout << compagnie6.getCode() << endl;
  //Compagnie7------------------------------------------
  Compagnie compagnie7("arm", 567, "noir", "blanc");
  compagnie7.afficherDonees();
  return 0;
}