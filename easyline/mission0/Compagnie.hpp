#ifndef COMPAGNIE_H
#define COMPAGNIE_H
#include <string>
using namespace std;
class Compagnie
{
private:
  string nom;
  int code;
  string couleur;
  string couleurSecondaire;
  int chiffreDAffaires;

public:
  Compagnie(string nom);
  Compagnie();
  Compagnie(string nom, string couleur, string couleurSecondaire);
  Compagnie(string nom, int code, string couleur, string couleurSecondaire);

  //Methode d'affichage des propriétés
  void afficherDonees();
  void setNom(string nom);
  void setCode(int code);
  void setCouleur(string couleurPrincipale);
  void setCouleurSecondaire(string couleurSecondaire);
  void setChiffreDAffaires(int chiffreDAffaires);
  string getNom();
  int getCode();
  string getCouleur();
  string getCouleurSecondaire();
  int getChiffreDAffaires();

  friend ostream &operator<<(ostream &, const Compagnie &); //pour avoir acces aux attributs d l'objet
};
#endif //COMPAGNIE_H