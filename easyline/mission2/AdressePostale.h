#ifndef ADRESSEPOSTALE_H
#define ADRESSEPOSTALE_H
#include <string>
using namespace std;

//Creation de class Adresse Postale
class AdressePostale
{

private:
  //Définition d'attributs
  string voie;
  string ville;
  int codePostal;

public:
  //Constructeur
  AdressePostale();
  AdressePostale(string voie, int codePostal, string ville);
  //Declaration des accesseurs
  string getVoie();
  string getVille();
  int getCodePostal();
  //Declaration des modificateurs
  void setVoie(string voie);
  void setCodePostal(int codePostal);
  void setVille(string Ville);
  //Methode d'affichage
  void afficherAdresse();
  friend class Voyageur;
};

#endif //ADRESSEPOSTALE_H