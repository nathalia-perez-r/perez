#ifndef VOYAGEUR_H
#define VOYAGEUR_H
#include <string>
#include "AdressePostale.hpp"
#include "Bagage.hpp"
using namespace std;

//Creation de class Voyageur
class Voyageur
{

private:
    //Définition d'attributs
    string nom;
    int age;
    string categorie;
    AdressePostale Adresse;
    Bagage BagageV;

public:
    //Constructeurs
    Voyageur();
    Voyageur(string nom, int age);
    //Declaration des accesseurs
    string getNom();
    int getAge();
    string getCategorie();
    AdressePostale getAdresse();

    //Declaration des modificateurs
    void setNom(string nom);
    void setAge(int age);
    void setAddresse(AdressePostale Adresse);
    void setBagageV(Bagage Bagage);
    //Methode d'affichage
    void afficherDonnees();
};

#endif //VOYAGEUR_H