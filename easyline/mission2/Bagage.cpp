#include <iostream>
#include <string>
#include "Bagage.hpp"
using namespace std;

//Definition de constructeur
Bagage::Bagage() : numero(""), couleur(""), poids("") {}
Bagage::Bagage(string numero, string couleur, string poids) : numero(numero), couleur(couleur), poids(poids) {}

//Definition des acceseurs
string Bagage::getNumero() { return this->numero; }
string Bagage::getCouleur() { return this->couleur; }
string Bagage::getPoids() { return this->poids; }

//Definition des modificateurs
void Bagage::setNumero(string numero) { this->numero = numero; }
void Bagage::setCouleur(string couleur) { this->couleur = couleur; }
void Bagage::setPoids(string poids) { this->poids = poids; }

//Definition fonction d'affichage
void Bagage::afficherBagage()
{
  cout << "Bagage: " << this->getNumero() << ", Couleur: " << this->getCouleur();
  cout << ", Poids: " << this->getPoids() << endl;
};
