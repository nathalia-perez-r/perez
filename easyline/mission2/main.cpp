#include <iostream>
#include <string>
#include "Voyageur.hpp"
#include "AdressePostale.hpp"
#include "Voyageur.cpp"
#include "AdressePostale.cpp"
#include "Bagage.hpp"
#include "Bagage.cpp"
using namespace std;

int main()
{
  Voyageur Voyageur1("Claire", 25);
  AdressePostale Adresse1("20 av erlk", 75000, "Paris");
  Voyageur1.setAddresse(Adresse1);
  Voyageur1.afficherDonnees();
  Adresse1.setVille("Rome");
  Voyageur1.setAddresse(Adresse1);
  Voyageur1.afficherDonnees();
  Bagage Bagage1("132", "noir", "19Kg");
  Voyageur1.setBagageV(Bagage1);
  Voyageur1.afficherDonnees();
  Bagage1.setCouleur("rouge");
  Voyageur1.setBagageV(Bagage1);
  Voyageur1.afficherDonnees();
  return 0;
}