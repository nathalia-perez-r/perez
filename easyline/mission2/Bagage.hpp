#ifndef BAGAGE_H
#define BAGAGE_H
#include <string>
using namespace std;

//Creation de class Bagage
class Bagage
{

private:
  //Définition d'attributs
  string numero;
  string couleur;
  string poids;

public:
  //Constructeur
  Bagage();
  Bagage(string numero, string couleur, string poids);
  //Declaration des accesseurs
  string getNumero();
  string getCouleur();
  string getPoids();
  //Declaration des modificateurs
  void setNumero(string numero);
  void setCouleur(string couleur);
  void setPoids(string poids);
  //Methode d'affichage
  void afficherBagage();
};

#endif //BAGAGE_H