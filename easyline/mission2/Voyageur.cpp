#include <iostream>
#include <string>
#include "Voyageur.hpp"
#include "AdressePostale.hpp"
#include "Bagage.hpp"
using namespace std;

//Definition des constructeurs par defaut et à deux arguments
Voyageur::Voyageur() : nom("VoyageurX"), age(20) {}
Voyageur::Voyageur(string voyageur, int ageV) : nom(voyageur), age(ageV) {}

//Definition des accesseurs
string Voyageur::getNom() { return this->nom; }
int Voyageur::getAge() { return this->age; }
string Voyageur::getCategorie() { return this->categorie; }
//Definition des modificateurs
void Voyageur::setNom(string nomV) { this->nom = nomV; }
void Voyageur::setAge(int ageV)
{
    this->age = ageV;
    if (age <= 1)
    {
        this->categorie = "Nourrison";
    }
    else if (age > 1 && age < 17)
    {
        this->categorie = "Enfant";
    }
    else if (age > 18 && age < 60)
    {
        this->categorie = "Adulte";
    }
    else
    {
        this->categorie = "Senior";
    }
}
void Voyageur::setAddresse(AdressePostale Adresse)
{
    this->Adresse = Adresse;
}
void Voyageur::setBagageV(Bagage BagageV)
{
    this->BagageV = BagageV;
}
//Definition methode d'affichage
void Voyageur::afficherDonnees()
{
    cout << "Voyageur : " << this->getNom() << endl;
    cout << "Age: " << this->getAge() << endl;
    cout << "Categorie: " << this->getCategorie() << endl;
    Adresse.afficherAdresse();
    BagageV.afficherBagage();
}
