#include <iostream>
#include <string>
#include "AdressePostale.hpp"
using namespace std;

//Definition de constructeur
AdressePostale::AdressePostale() : voie("non indiqué"), codePostal(75000), ville("Paris") {}
AdressePostale::AdressePostale(string nomVoie, int codePstl, string ville) : voie(nomVoie), codePostal(codePstl), ville(ville) {}

//Definition des acceseurs
string AdressePostale::getVoie() { return this->voie; }
int AdressePostale::getCodePostal() { return this->codePostal; }
string AdressePostale::getVille() { return this->ville; }

//Definition des modificateurs
void AdressePostale::setVoie(string voie) { this->voie = voie; }
void AdressePostale::setCodePostal(int codePostal) { this->codePostal = codePostal; }
void AdressePostale::setVille(string ville) { this->ville = ville; }

//Definition fonction d'affichage
void AdressePostale::afficherAdresse()
{
  cout << "Adresse: " << this->getVoie() << ", " << this->getCodePostal();
  cout << ", " << this->getVille() << endl;
};
